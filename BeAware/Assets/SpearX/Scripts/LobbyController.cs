﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Photon.Realtime;
using Photon.Pun;
using TMPro;


public class LobbyController : MonoBehaviourPunCallbacks
{
    [SerializeField]
    private Text buttonText;
    [SerializeField]
    private int roomSize;
    private bool connected;
    private bool starting;
    public GameObject roomButton;
    public List<RoomInfo> roomList;
    public GameObject canvas;
    public int roomNumber;

    public override void OnConnectedToMaster()
    {
        PhotonNetwork.AutomaticallySyncScene = true;
        connected = true;
        buttonText.text = "Begin Game";
        Debug.Log("Begin Game!");
        //soldo
        PhotonNetwork.JoinLobby();
    }

    public void GameButton()
    {
        if (connected)
        {
            if (!starting)
            {
                starting = true;
                buttonText.text = "Starting game...";
                //PhotonNetwork.JoinRandomRoom();
                CreateRoom();
            }
            else
            {
                starting = false;
                buttonText.text = "Begin Game";
                PhotonNetwork.LeaveRoom();
            }
        }
        else
        {
            Debug.Log("Not connected to a server");
        }
    }

    private void CreateRoom()
    {
        Debug.Log("Creating room now...");
        int randomRoomNumber = Random.Range(100000, 999999);
        roomNumber = randomRoomNumber;
        RoomOptions roomOptions = new RoomOptions() { IsVisible = true, IsOpen = true, MaxPlayers = (byte) roomSize };
        PhotonNetwork.CreateRoom($"Room{randomRoomNumber}", roomOptions);
        Debug.Log(randomRoomNumber);
    }

    public override void OnJoinRandomFailed(short returnCode, string message)
    {
        Debug.Log("Failed to join a room. Creating one!");
        CreateRoom();
    }


    public override void OnCreateRoomFailed(short returnCode, string message)
    {
        Debug.Log("Failed to create a room");
        CreateRoom();
    }

    public void ClearRoomList()
    {
        Transform content = canvas.transform.Find("Scroll View/Viewport/Content");
        foreach (Transform t in content)
        {
            Destroy(t.gameObject);
        }
    }

    public void AutoRefreshList()
    {
        ClearRoomList();
        Transform content = canvas.transform.Find("Scroll View/Viewport/Content");
        foreach (RoomInfo info in roomList)
        {
            GameObject newGameRoomButton = Instantiate(roomButton, content) as GameObject;

            newGameRoomButton.transform.Find("Name").GetComponent<TMP_Text>().text = info.Name;
            newGameRoomButton.transform.Find("Players").GetComponent<TMP_Text>().text = $"{info.PlayerCount}/{info.MaxPlayers}";

            newGameRoomButton.GetComponent<Button>().onClick.AddListener(delegate { JoinRoom(newGameRoomButton.transform); });
        }
    }

    public override void OnRoomListUpdate(List<RoomInfo> p_roomList)
    {
        ClearRoomList();
        roomList = p_roomList;

        Debug.Log($"Loaded rooms @ {Time.time}");

        Transform content = canvas.transform.Find("Scroll View/Viewport/Content");
        foreach (RoomInfo info in roomList)
        {
            GameObject newGameRoomButton = Instantiate(roomButton, content) as GameObject;

            newGameRoomButton.transform.Find("Name").GetComponent<TMP_Text>().text = info.Name;
            newGameRoomButton.transform.Find("Players").GetComponent<TMP_Text>().text = $"{info.PlayerCount}/{info.MaxPlayers}";

            newGameRoomButton.GetComponent<Button>().onClick.AddListener(delegate { JoinRoom(newGameRoomButton.transform); });
        }

        base.OnRoomListUpdate(roomList);
    }

    private void JoinRoom(Transform p_button)
    {
        Debug.Log($"Joining rooms @ {Time.time}");
        string t_roomName = p_button.transform.Find("Name").GetComponent<TMP_Text>().text;
        PhotonNetwork.JoinRoom(t_roomName);
    }

    //public override void OnDisconnected(DisconnectCause cause)
    //{
    //    PhotonNetwork.LoadLevel(0);
    //    base.OnDisconnected(cause);
    //}
}
