using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class RoofController : MonoBehaviour
{
    [SerializeField]
    public SpriteRenderer graphic;

    public void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            FadeIn();
        }
    }

    public void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            FadeOut();
        }
    }
    // Start is called before the first frame update
    void FadeIn()
    {
        graphic.DOFade(0, 2f);
    }

    // Update is called once per frame
    void FadeOut()
    {
        graphic.DOFade(1, 1f);
    }
}
