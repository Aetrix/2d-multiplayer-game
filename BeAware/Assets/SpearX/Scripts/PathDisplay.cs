﻿using Photon.Pun;
using Photon.Realtime;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;


namespace Pathfinding
{
    public class PathDisplay : MonoBehaviour
    {
        public LineRenderer lineRenderer;
        public Seeker seeker;

        public AIDestinationSetter setter;
        public PhotonView PV;
        // Start is called before the first frame update
        void Start()
        {

            if (seeker == null)
            {
                seeker = GetComponent<Seeker>();
               
            }
            if (setter == null)
            {
                setter = GetComponent<AIDestinationSetter>();
            }

           
             
        }

        public void OnTriggerEnter2D(Collider2D collision)
        {
            if (collision.gameObject.tag == "Player")
            {
                if (setter.target == null)
                {
                    setter.target = collision.gameObject.transform;
                }
            }
        }

        public void OnTriggerExit2D(Collider2D collision)
        {
            if (collision.gameObject.tag == "Player")
            {
                setter.target = null;
            }
        }

        // Update is called once per frame
        void Update()
        {
            ////Debug.Log(seeker.GetCurrentPath().vectorPath.ToArray().Length);
            //lineRenderer.positionCount = seeker.GetCurrentPath().vectorPath.ToArray().Length;
            //if (seeker.GetCurrentPath().vectorPath.Count > 2)
            //{

            //    for (int i = 0; i < seeker.GetCurrentPath().vectorPath.Count; i++)
            //    {
            //        //lineRenderer.SetPosition(i, new Vector3(seeker.GetCurrentPath().vectorPath[i].x, seeker.GetCurrentPath().vectorPath[i].y, seeker.GetCurrentPath().vectorPath[i].z));
            //        lineRenderer.SetPosition(i, (Vector3)seeker.GetCurrentPath().path[i].position);
               
            //    }
            //}
            //else
            //{
            //    lineRenderer.positionCount = 0;
            //}
        }
    }
}