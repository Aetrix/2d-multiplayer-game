using System.Collections;
using System.Collections.Generic;
using System.IO;
using Photon.Pun;
using UnityEngine;

public class LampInstantiate : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        var lamp = PhotonNetwork.Instantiate(Path.Combine("PhotonPrefabs", "Lamp"), this.gameObject.transform.position, Quaternion.identity);
        lamp.gameObject.transform.parent = this.transform;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
