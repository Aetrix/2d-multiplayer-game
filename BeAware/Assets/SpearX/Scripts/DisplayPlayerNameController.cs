using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using Photon.Realtime;
using UnityEngine.UI;
using TMPro;

public class DisplayPlayerNameController : MonoBehaviour
{
    public TMP_Text playerNameText;
    public Canvas playerCanvas;
    private PhotonView PV;

    public Button counterButton;
    //public Button decreaseButton;
    public int counter;
    public TMP_Text counterText;


    // Start is called before the first frame update
    void Start()
    {
        PV = GetComponent<PhotonView>();
        playerCanvas.worldCamera = Camera.main;
        if (PV.IsMine)
        {
            playerNameText.text = PhotonNetwork.NickName;
            counterText.text = counter.ToString();
            counterButton = GameObject.Find("CounterButton").GetComponent<Button>();
            counterButton.onClick.AddListener(delegate { CallCounter(); });

            //decreaseButton = GameObject.Find("CounterButton").GetComponent<Button>();
            //decreaseButton.onClick.AddListener(delegate { CallDecreaseCounter(); });

        }
        else
        {
            playerNameText.text = PV.Owner.NickName;
            counterText.text = counter.ToString();

        }
    }

    [PunRPC]
    public void Counter()
    {
        counter += 1;
        counterText.text = counter.ToString();
    }

    
    public void CallCounter()
    {
        PV.RPC("Counter", RpcTarget.AllBuffered);
    }

    [PunRPC]
    public void DecreaseCounter()
    {
        if (counter > 0)
        {
            counter -= 1;
        }
        counterText.text = counter.ToString();
    }


    public void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Collectable")
        {
            PhotonView pv = collision.gameObject.GetComponent<PhotonView>();
            if (PV.IsMine)
            {
                CallCounter();
                
                if (pv.Owner != PhotonNetwork.LocalPlayer)
                {
                    pv.SetOwnerInternal(PhotonNetwork.LocalPlayer, PV.OwnerActorNr);
                    PhotonNetwork.Destroy(pv.gameObject);
                }
            }
        }

        if (collision.gameObject.tag == "Interactable")
        {
            PhotonView pv = collision.gameObject.GetComponent<PhotonView>();
            if (PV.IsMine)
            {
                var boo = pv.gameObject.GetComponentInChildren<Animator>();
               
                pv.SetOwnerInternal(PhotonNetwork.LocalPlayer, PV.OwnerActorNr);
                boo.SetBool("Lightened", !boo.GetBool("Lightened"));
                
            }
        }
    }

    public void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Hostile")
        {
            if (PV.IsMine)
            {
                PV.RPC("DecreaseCounter", RpcTarget.AllBuffered);
            }
        }
    }
}
