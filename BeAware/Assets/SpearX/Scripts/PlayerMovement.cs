﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using Cinemachine;
using UnityEngine.UI;
using TMPro;

public class PlayerMovement : MonoBehaviour
{
    public float playerSpeed = 4f;
    private PhotonView PV;
    [SerializeField]
    private VariableJoystick variableJoystick;
    private Animator anim;
    private CinemachineVirtualCamera vCamera;
    private Rigidbody2D playerRb;
    
    // Start is called before the first frame update
    void Start()
    {
        anim = GetComponent<Animator>();
        PV = GetComponent<PhotonView>();
        vCamera = FindObjectOfType<CinemachineVirtualCamera>();
        playerRb = GetComponent<Rigidbody2D>();
        if (PV.IsMine)
        {
            vCamera.Follow = this.gameObject.transform;
            
        }
        
        variableJoystick = FindObjectOfType<VariableJoystick>();
        
    }


    // Update is called once per frame
    void Update()
    {
        if (PV.IsMine)
        {
            Movement();
        }

        if (Input.GetKeyDown(KeyCode.Escape))
            Application.Quit();
    }

    void Movement()
    {
        //Vector3 direction = Vector3.forward * variableJoystick.Vertical + Vector3.right * variableJoystick.Horizontal;
#if UNITY_ANDROID
        float yMov = variableJoystick.Vertical;
        float xMov = variableJoystick.Horizontal;
#endif
#if UNITY_STANDALONE
        float yMov = Input.GetAxis("Vertical");
        float xMov = Input.GetAxis("Horizontal");
#endif
        anim.SetFloat("Horizontal", xMov);
        anim.SetFloat("Vertical", yMov);
        playerRb.velocity = new Vector2(xMov * playerSpeed, yMov * playerSpeed);
        //transform.position += new Vector3(xMov / 7.5f, yMov / 7.5f, 0);

        //transform.position += new Vector3(xMov / 7.5f, yMov / 7.5f, 0);
    }
//    void Movement()
//    {
//        //Vector3 direction = Vector3.forward * variableJoystick.Vertical + Vector3.right * variableJoystick.Horizontal;
//#if UNITY_ANDROID
//        float yMov = variableJoystick.Vertical;
//        float xMov = variableJoystick.Horizontal;
//#endif
//#if UNITY_STANDALONE
//        float yMov = Input.GetAxis("Vertical");
//        float xMov = Input.GetAxis("Horizontal");
//#endif
//        anim.SetFloat("Horizontal", xMov);
//        anim.SetFloat("Vertical", yMov);
//        transform.position += new Vector3(xMov / 7.5f, yMov / 7.5f, 0);

//        //transform.position += new Vector3(xMov / 7.5f, yMov / 7.5f, 0);
//    }
}
