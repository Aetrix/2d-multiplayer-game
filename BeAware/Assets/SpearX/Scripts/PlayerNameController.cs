using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using UnityEngine.UI;


public class PlayerNameController : MonoBehaviour
{
	public InputField inputField;

    const string playerNamePrefix = "DefaultName";

    // Start is called before the first frame update
    void Start()
    {
        string defaultName = string.Empty;
        if (inputField==null)
        {
            if (PlayerPrefs.HasKey(playerNamePrefix))
            {
                defaultName = PlayerPrefs.GetString(playerNamePrefix);
                inputField.text = defaultName;
            }
        }
        PhotonNetwork.NickName = defaultName;
    }

    // Update is called once per frame
    public void SetPlayerName(string value)
    {
        if (string.IsNullOrEmpty(value))
        {
            Debug.LogError("Player Name is Null or Empty");
        }
        PhotonNetwork.NickName = value;

        PlayerPrefs.SetString(playerNamePrefix, value);
    }
}
